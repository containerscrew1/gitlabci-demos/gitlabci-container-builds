# Example

FROM docker.io/debian:buster-slim

RUN set -eux ;\
    apt-get update ;\
    apt-get install wget -y ;\
    rm -rf /var/lib/apt/lists/*

WORKDIR /app
#Fake
ENTRYPOINT ["wget"]
